.PHONY: all

DEFAULT_TARGET: all

TF_BIN ?= terraform

all: init plan

plan:
	cd instances/$(ENV) && terraform plan

apply:
	 cd instances/$(ENV) && terraform apply $(AUTO)

destroy:
	cd instances/$(ENV) && terraform destroy $(AUTO)

init:
	utils/env_setup.sh create_spaces_bucket $(ENV)
	utils/env_setup.sh prepare_deploy_links $(ENV) $(DIALER)

backup:
	doctl auth init
	utils/env_setup.sh backup $(ENV)

restore:
	doctl auth init
	utils/env_setup.sh restore $(ENV)

delete:
	rm -r instances/$(ENV)
