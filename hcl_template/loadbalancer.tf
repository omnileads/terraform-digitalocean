resource "digitalocean_certificate" "omlcert" {
  name                        = format("%s.%s", var.omlapp_hostname, var.domain_name) 
  type                        = "lets_encrypt"
  domains                     = [format("%s.%s", var.omlapp_hostname, var.domain_name)]
}

module "lb" {
  source                      = "../../modules/loadbalancer"
  name                        = var.name_lb
  tenant                      = var.oml_tenant_name
  environment                 = var.environment
  region                      = var.region
  tls_passthrough             = false
  vpc_id                      = module.vpc.id
  target_droplets             = [module.droplet_omlapp.id[0]]
  target_port                 = var.omlapp_nginx_port
  ssl_cert                    = digitalocean_certificate.omlcert.name
}

resource "digitalocean_record" "omlapp_lb" {
  domain                      = var.domain_name
  type                        = "A"
  name                        = var.name
  value                       = module.lb.lb_ip
}
