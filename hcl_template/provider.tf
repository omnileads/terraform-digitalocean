terraform {
    required_version = ">= 1.0.0"

    required_providers {
      digitalocean = {
        source      = "digitalocean/digitalocean"
        version     = "~> 2.0"
      }
    }

# uncomment if you want to save tfstate on digitalocean spaces - S3
    backend "s3" {
      bucket                      = "tfstatebucket" #Your tenant string identifier
      key                         = "customer-name-terraform.tfstate" #Your tenant tfstate string identifier
      region                      = "us-east-1" #AWS S3 region
      endpoint                    = "spaces-region.digitaloceanspaces.com" #Your Spaces URL
      access_key                  = "spaces-key-id" #Your Spaces access key
      secret_key                  = "spaces-key-secret" #Your Spaces secret key
      skip_credentials_validation = true
      skip_metadata_api_check     = true
    }

}

provider "digitalocean" {

  token             = var.digitalocean_token

  spaces_access_id  = var.spaces_key
  spaces_secret_key = var.spaces_secret_key

}

