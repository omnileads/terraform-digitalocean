#  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet
#  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet #  RTPENGINE componenet

  module "droplet_rtpengine" {
  source                 = "../../modules/droplet"
  image_name             = var.img_centos
  name                   = var.name_rtpengine
  tenant                 = var.oml_tenant_name
  environment            = var.environment
  # droplet_count       = var.droplet_count
  region                 = var.region
  ssh_keys               = [var.ssh_key_fingerprint]
  vpc_uuid               = module.vpc.id
  droplet_size           = var.droplet_rtp_size
  monitoring             = false
  private_networking     = true
  user_data              = templatefile("../../modules/omlutilities/first_boot_installer_omlrtpengine.tpl", {
    oml_infras_stage        = var.cloud_provider
    oml_type_deploy         = "CLOUD"
    oml_rtpengine_release   = var.oml_rtpengine_branch
    oml_nic                 = var.droplet_nic
    oml_public_nic          = "NULL"
  })
  }

  