# infra

variable "region" {}
# variable "vpc_cidr" {}
variable "domain_name" {}
variable "name" {}
variable "oml_tenant_name" {}
variable "environment" {}
variable "ssh_key_fingerprint" {}

variable "digitalocean_token" {}

variable "cloud_provider" {}
variable "droplet_nic" {}
variable "callrec_storage" {}

variable "spaces_key" {}
variable "spaces_secret_key" {}
variable "spaces_url" {}

variable "nfs_host" {}

variable "init_environment" {}
variable "reset_admin_pass" {}

variable "name_rtpengine" {}
variable "name_pgsql" {}
variable "name_redis" {}
variable "name_mariadb" {}
variable "name_wombat" {}
variable "name_omlapp" {}
variable "name_kamailio" {}
variable "name_asterisk" {}
variable "name_websocket" {}
variable "name_lb" {}
variable "name_haproxy" {}

variable "oml_app_branch" {}
variable "oml_acd_branch" {}
variable "oml_redis_branch" {}
variable "oml_rtpengine_branch" {}
variable "oml_kamailio_branch" {}
variable "oml_ws_branch" {}

variable "oml_app_img" {}
variable "oml_acd_img" {}
variable "oml_redis_img" {}
variable "oml_rtpengine_img" {}
variable "oml_kamailio_img" {}
variable "oml_ws_img" {}
variable "oml_nginx_img" {}

variable "app" {}
variable "droplet_oml_size" {}
variable "droplet_asterisk_size" {}
variable "droplet_rtp_size" {}
variable "droplet_dialer_size" {}
variable "droplet_mariadb_size" {}
variable "droplet_kamailio_size" {}
variable "droplet_redis_size" {}
variable "droplet_websocket_size" {}
#variable "droplet_postgresql_size" {}
variable "pgsql_size" {}
#variable "droplet_haproxy_size" {}
variable "img_centos" {}
variable "img_ubuntu" {}
variable "img_docker" {}
variable "ssh_key_file" {
  default = "~/.ssh/id_rsa.pub"
}
# App # App # App
variable "ssh_allowed_ip" {
  type    = list(string)
}

variable "sip_allowed_ip" {
  type    = list(string)
}

variable "cluster_db_nodes" {}

# OMniLeads deploy vars

variable "omlapp_nginx_port" {}
variable "oml_tz" {}
variable "omlapp_hostname" {}
variable "ami_user" {}
variable "ami_password" {}
variable "dialer_user" {}
variable "dialer_password" {}
variable "pg_database" {}
variable "pg_username" {}
variable "pg_password" {}
variable "sca" {}
variable "ecctl" {
  default = "28800"
}
variable "schedule" {
  default = "agenda"
}
variable "extern_ip" {
  default = "none"
}
variable "kamailio_shm_size" {
  default = "64"
}
variable "kamailio_pkg_size" {
  default = "8"
}



# Wombat dialer
variable "wombat_database" {}
variable "wombat_database_username" {}
variable "wombat_database_password" {}

variable "oml_app_backup_filename" {}
variable "oml_acd_backup_filename" {}

variable "oml_backup_path" { default = "" }
variable "oml_auto_restore" {}

variable "oml_app_repo_url" { 
  default = "https://gitlab.com/omnileads/ominicontacto.git"
}

variable "oml_high_load" {}
