
# #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet
# #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet #  PGSQL componenet

  module "pgsql"  {
    source        = "../../modules/db"
    name          = var.name_pgsql
    tenant        = var.oml_tenant_name
    environment   = var.environment
    engine        = "pg"
    db_version    = "11"
    size          = var.pgsql_size
    region        = var.region
    vpc_id        = module.vpc.id
    cluster_nodes = var.cluster_db_nodes
  }
  
  resource "digitalocean_database_user" "omnileads" {
    cluster_id = module.pgsql.database_id
    name       = var.pg_username
  }
  
  resource "digitalocean_database_db" "database-omlapp" {
    cluster_id = module.pgsql.database_id
    name       = var.pg_database
  }
