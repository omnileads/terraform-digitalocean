  # Firewall aplicado al droplet ASTERISK # Firewall aplicado al droplet ASTERISK
  # Firewall aplicado al droplet ASTERISK # Firewall aplicado al droplet ASTERISK

  resource "digitalocean_firewall" "fw_asterisk" {
    name = var.name_asterisk

    droplet_ids = [module.droplet_asterisk.id[0]]

    # SSH allowed IPs
    dynamic "inbound_rule" {
      iterator = ssh_allowed_ip
      for_each = var.ssh_allowed_ip
      content {
        port_range       = "22"
        protocol         = "tcp"
        source_addresses = var.ssh_allowed_ip
      }
    }

    # SIP trunks ASTERISK
    dynamic "inbound_rule" {
      iterator = sip_allowed_ip
      for_each = var.sip_allowed_ip
      content {
        port_range       = "5161"
        protocol         = "udp"
        source_addresses = var.sip_allowed_ip
      }
    }
    # RTP trunks ASTERISK
    dynamic "inbound_rule" {
      iterator = sip_allowed_ip
      for_each = var.sip_allowed_ip
      content {
        port_range       = "20000-30000"
        protocol         = "udp"
        source_addresses = var.sip_allowed_ip
      }
    }

    inbound_rule {
      protocol                  = "udp"
      port_range                = "20000-30000"
      source_droplet_ids        = [module.droplet_rtpengine.id[0]]
    }

    inbound_rule {
      protocol                  = "udp"
      port_range                = "5160"
      source_droplet_ids        = [module.droplet_kamailio.id[0]]
    }

    # ASTERISK AMI from Wombat
    inbound_rule {
      protocol                  = "tcp"
      port_range                = "5038"
      source_droplet_ids        = [module.droplet_omlapp.id[0]]
    }

    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
    protocol                = "icmp"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
    }

  }
