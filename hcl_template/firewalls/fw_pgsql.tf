# Firewall aplicado al cluster PGSQL # Firewall aplicado al cluster PGSQL
  # Firewall aplicado al cluster PGSQL # Firewall aplicado al cluster PGSQL
  
  resource "digitalocean_database_firewall" "pgsql-fw" {
    cluster_id = module.pgsql.database_id
  
    rule {
      type  = "droplet"
      value = module.droplet_omlapp.id[0]
    }

    rule {
      type  = "droplet"
      value = module.droplet_asterisk.id[0]
    }
  }
