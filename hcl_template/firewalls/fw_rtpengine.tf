# Firewall aplicado al droplet rtpengine # Firewall aplicado al droplet rtpengine
  # Firewall aplicado al droplet rtpengine # Firewall aplicado al droplet rtpengine


  resource "digitalocean_firewall" "fw_rtpengine" {
    name = var.name_rtpengine

    droplet_ids = [module.droplet_rtpengine.id[0]]

    # SSH allowed IPs
    dynamic "inbound_rule" {
      iterator = ssh_allowed_ip
      for_each = var.ssh_allowed_ip
      content {
        port_range       = "22"
        protocol         = "tcp"
        source_addresses = var.ssh_allowed_ip
      }
    }
    inbound_rule {
      protocol            = "tcp"
      port_range          = "22222"
      source_droplet_ids  = [module.droplet_kamailio.id[0]]
    }
    inbound_rule {
      protocol            = "udp"
      port_range          = "20000-50000"
      source_addresses    = ["0.0.0.0/0"]
    }

    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
    }
  }
