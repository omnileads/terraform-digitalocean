# Firewall aplicado al droplet websocket # Firewall aplicado al droplet websocket
  # Firewall aplicado al droplet websocket # Firewall aplicado al droplet websocket


  resource "digitalocean_firewall" "fw_websocket" {
    name = var.name_websocket

    droplet_ids = [module.droplet_websocket.id[0]]

    # SSH allowed IPs
    dynamic "inbound_rule" {
      iterator = ssh_allowed_ip
      for_each = var.ssh_allowed_ip
      content {
        port_range       = "22"
        protocol         = "tcp"
        source_addresses = var.ssh_allowed_ip
      }
    }

    inbound_rule {
      protocol            = "tcp"
      port_range          = "8000"
      source_droplet_ids  = [module.droplet_omlapp.id[0]]
    }

    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  }
