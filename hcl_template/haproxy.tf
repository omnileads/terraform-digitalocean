#  haproxy componenet #  haproxy componenet #  haproxy componenet #  haproxy componenet #  haproxy componenet
#  haproxy componenet #  haproxy componenet #  haproxy componenet #  haproxy componenet #  haproxy componenet

  module "droplet_haproxy"  {
   source             = "../../modules/droplet"
   image_name         = var.img_centos
   name               = var.name_haproxy
   tenant             = var.oml_tenant_name
   environment        = var.environment
   region             = var.region
   ssh_keys           = [var.ssh_key_fingerprint]
   vpc_uuid           = module.vpc.id
   droplet_size       = var.droplet_haproxy_size
   monitoring         = false
   private_networking = true
   ipv6               = false
   user_data          = templatefile("../../modules/user_data_haproxy.tpl", {
     asterisk_host             = module.droplet_asterisk.ipv4_address_private
   })
  }

  # Firewall aplicado al droplet haproxy # Firewall aplicado al droplet haproxy
  # Firewall aplicado al droplet haproxy # Firewall aplicado al droplet haproxy


  resource "digitalocean_firewall" "fw_haproxy" {
    name = var.name_haproxy

    droplet_ids = [module.droplet_haproxy.id[0]]


    inbound_rule {
      protocol              = "tcp"
      port_range            = "22"
      source_addresses      = ["0.0.0.0/0"]
    }
    # haproxy AMI from Wombat and OMLApp
    inbound_rule {
      protocol                  = "tcp"
      port_range                = "5038"
      source_droplet_ids        = [module.droplet_omlapp.id[0],module.droplet_wombat.id[0]]
    }

    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }
    outbound_rule {
    protocol                = "icmp"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
    }

  }
