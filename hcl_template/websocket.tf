#  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet
#  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet #  WEBSOCKET componenet

  module "droplet_websocket" {
  source             = "../../modules/droplet"
  image_name         = var.img_centos
  name               = var.name_websocket
  tenant             = var.oml_tenant_name
  environment        = var.environment
  # droplet_count      = var.droplet_count
  region             = var.region
  ssh_keys           = [var.ssh_key_fingerprint]
  vpc_uuid           = module.vpc.id
  droplet_size       = var.droplet_rtp_size
  monitoring         = false
  private_networking = true
  user_data          = templatefile("../../modules/omlutilities/first_boot_installer_ws.tpl", {
    oml_infras_stage              = var.cloud_provider
    oml_nic                       = var.droplet_nic
    oml_ws_release                = var.oml_ws_branch
    oml_redis_host                = module.droplet_redis.ipv4_address_private
    oml_redis_port                = 6379
    oml_ws_port                   = 8000
  })
  }

  