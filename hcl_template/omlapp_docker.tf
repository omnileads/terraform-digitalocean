# OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data
# OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data

data "template_file" "omlapp" {
  template = file("../../modules/omlutilities/oml_docker.tpl")
  vars = {
    oml_infras_stage              = var.cloud_provider
    oml_app_release               = var.oml_app_branch
    oml_app_img                   = var.oml_app_img
    oml_acd_img                   = var.oml_acd_img
    oml_kamailio_img              = var.oml_kamailio_img
    oml_redis_img                 = var.oml_redis_img
    oml_nginx_img                 = var.oml_nginx_img
    oml_ws_img                    = var.oml_ws_img
    oml_tenant_name               = var.oml_tenant_name
    oml_callrec_device            = var.callrec_storage
    s3_access_key                 = var.spaces_key
    s3_secret_key                 = var.spaces_secret_key
    s3url                         = var.spaces_url
    s3_bucket_name                = var.spaces_bucket_name
    nfs_host                      = "NULL"
    oml_nic                       = var.droplet_nic
    oml_ami_user                  = var.ami_user
    oml_ami_password              = var.ami_password
    oml_pgsql_port                = "NULL"
    oml_pgsql_host                = module.droplet_postgresql.ipv4_address_private
    oml_pgsql_db                  = var.pg_database
    oml_pgsql_user                = var.pg_username
    oml_pgsql_password            = var.pg_password
    oml_pgsql_cloud               = "true"
    api_dialer_user               = var.dialer_user
    api_dialer_password           = var.dialer_password
    oml_dialer_host               = module.droplet_wombat.ipv4_address_private
    oml_rtpengine_host            = module.droplet_rtpengine.ipv4_address_private
    oml_extern_ip                 = var.extern_ip
    oml_tz                        = var.oml_tz
    oml_app_sca                   = var.sca
    oml_app_ecctl                 = var.ecctl
    oml_app_login_fail_limit      = 10    
    oml_app_init_env              = var.init_environment
    oml_app_reset_admin_pass      = "true"
    oml_app_install_sngrep        = "false"
  }
}

#  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet
#  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet

  module "droplet_omlapp" {
  source                      = "../../modules/droplet"
  image_name                  = var.img_docker
  name                        = var.name_omlapp
  tenant                      = var.oml_tenant_name
  environment                 = var.environment
  # droplet_count      = var.droplet_count
  region                      = var.region
  ssh_keys                    = [var.ssh_key_fingerprint]
  vpc_uuid                    = module.vpc.id
  droplet_size                = var.droplet_oml_size
  monitoring                  = false
  private_networking          = true
  ipv6                        = false
  user_data                   = data.template_file.omlapp.rendered
  }

  # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp
  # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp # Firewall aplicado al droplet omlApp

  resource "digitalocean_firewall" "fw_omlapp" {
    name = var.name_omlapp

    droplet_ids = [module.droplet_omlapp.id[0]]

    ############ INBOUND ######################
    # SSH all
    inbound_rule {
      protocol         = "tcp"
      port_range       = "22"
      source_addresses = ["0.0.0.0/0"]
    }
    # Web App all
    inbound_rule {
      protocol                  = "tcp"
      port_range                = "443"
      source_load_balancer_uids = [module.lb.lb_id]
    }
    # WOMBAT all
    inbound_rule {
      protocol            = "tcp"
      port_range          = "8080"
      source_addresses = ["0.0.0.0/0"]
    }

    ############ OUTBOUND ######################
    outbound_rule {
      protocol              = "tcp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
      protocol              = "udp"
      port_range            = "1-65535"
      destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  }
