# OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data
# OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data OMLAPP user_data

data "template_file" "omlapp" {
  template = file("../../modules/omlutilities/first_boot_installer_omlapp.tpl")
  vars = {
    aws_region                    = "NULL"
    iam_role_name                 = "NULL"
    oml_infras_stage              = var.cloud_provider
    oml_app_release               = var.oml_app_branch
    oml_tenant_name               = var.oml_tenant_name
    oml_callrec_device            = var.callrec_storage
    s3_access_key                 = var.spaces_key
    s3_secret_key                 = var.spaces_secret_key
    s3url                         = var.spaces_url
    ast_bucket_name               = var.oml_tenant_name
    nfs_host                      = "NULL"
    optoml_device                 = "NULL"
    pgsql_device                  = "NULL"
    oml_nic                       = var.droplet_nic
    oml_ami_user                  = var.ami_user
    oml_ami_password              = var.ami_password
    oml_acd_host                  = module.droplet_asterisk.ipv4_address_private
    oml_pgsql_port                = module.pgsql.database_port
    oml_pgsql_host                = module.pgsql.database_private_host
    oml_pgsql_db                  = var.pg_database
    oml_pgsql_user                = var.pg_username
    oml_pgsql_password            = digitalocean_database_user.omnileads.password
    oml_pgsql_cloud               = "true"
    api_dialer_user               = var.dialer_user
    api_dialer_password           = var.dialer_password
    oml_dialer_host               = module.droplet_wombat.ipv4_address_private
    oml_rtpengine_host            = module.droplet_rtpengine.ipv4_address_private
    oml_kamailio_host             = module.droplet_kamailio.ipv4_address_private
    oml_redis_host                = module.droplet_redis.ipv4_address_private
    oml_websocket_host            = module.droplet_websocket.ipv4_address_private
    oml_websocket_port            = 8000
    oml_extern_ip                 = var.extern_ip
    oml_tz                        = var.oml_tz
    oml_app_sca                   = var.sca
    oml_app_ecctl                 = var.ecctl
    oml_app_login_fail_limit      = 10    
    oml_app_init_env              = var.init_environment
    oml_app_reset_admin_pass      = "true"
    oml_app_install_sngrep        = "false"
    oml_backup_filename           = var.oml_app_backup_filename
    oml_auto_restore              = var.oml_auto_restore
    oml_high_load                 = var.oml_high_load
  }
}

#  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet
#  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet #  OMLAPP componenet

  module "droplet_omlapp" {
  source                      = "../../modules/droplet"
  image_name                  = var.img_centos
  name                        = var.name_omlapp
  tenant                      = var.oml_tenant_name
  environment                 = var.environment
  # droplet_count      = var.droplet_count
  region                      = var.region
  ssh_keys                    = [var.ssh_key_fingerprint]
  vpc_uuid                    = module.vpc.id
  droplet_size                = var.droplet_oml_size
  monitoring                  = false
  private_networking          = true
  ipv6                        = false
  user_data                   = data.template_file.omlapp.rendered
  }


  