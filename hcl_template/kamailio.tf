#  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet
#  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet #  KAMAILIO componenet

  module "droplet_kamailio"  {
   source             = "../../modules/droplet"
   image_name         = var.img_centos
   name               = var.name_kamailio
   tenant             = var.oml_tenant_name
   environment        = var.environment
   region             = var.region
   ssh_keys           = [var.ssh_key_fingerprint]
   vpc_uuid           = module.vpc.id
   droplet_size       = var.droplet_kamailio_size
   monitoring         = false
   private_networking = true
   ipv6               = false
   user_data          = templatefile("../../modules/omlutilities/first_boot_installer_omlkam.tpl", {
     oml_infras_stage          = var.cloud_provider
     oml_nic                   = var.droplet_nic
     oml_kamailio_release      = var.oml_kamailio_branch
     oml_rtpengine_host        = module.droplet_rtpengine.ipv4_address_private
     oml_redis_host            = module.droplet_redis.ipv4_address_private
     oml_acd_host              = module.droplet_asterisk.ipv4_address_private
     oml_kamailio_pkg_size     = var.kamailio_pkg_size
     oml_kamailio_shm_size     = var.kamailio_shm_size
     oml_kamailio_host         = "NULL"
   })
  }
