#  REDIS componenet #  REDIS componenet #  REDIS componenet #  REDIS componenet #  REDIS componenet
#  REDIS componenet #  REDIS componenet #  REDIS componenet #  REDIS componenet #  REDIS componenet

  module "droplet_redis"  {
    source             = "../../modules/droplet"
    image_name         = var.img_centos
    name               = var.name_redis
    tenant             = var.oml_tenant_name
    environment        = var.environment
    region             = var.region
    ssh_keys           = [var.ssh_key_fingerprint]
    vpc_uuid           = module.vpc.id
    droplet_size       = var.droplet_redis_size
    monitoring         = false
    private_networking = true
    ipv6               = false
    user_data          = templatefile("../../modules/omlutilities/first_boot_installer_omlredis.tpl", {
      oml_infras_stage              = var.cloud_provider
      oml_nic                       = var.droplet_nic
      oml_redis_release             = var.oml_redis_branch
      oml_high_load                 = var.oml_high_load
   })
  }
