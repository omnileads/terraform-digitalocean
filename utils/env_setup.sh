#!/bin/bash

set -eo pipefail
PATH=$PATH:~/.local/bin
ENVS_DIR=./instances

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
S3CMD=$(which s3cmd)
GREP=$(which grep)

if [ -z "${1}" ]; then
  echo "Usage:"
  echo -e "\t> ./env_setup.sh prepare_deploy_links <environment>"
fi

prepare_deploy_links() {

  local environment=$1
  local dialer=$2
  local type=CLUSTER
  
  if [ ! -d ${ENVS_DIR}/${environment} ]; then
    mkdir ${ENVS_DIR}/${environment}/
    mkdir ${ENVS_DIR}/${environment}/backup_restore
  fi
  undo_links ${environment}
  cd ${ENVS_DIR}/${environment}/

  cp ../../hcl_template/vars.auto.tfvars ./
  cp ../../hcl_template/provider.tf ./

  sed -i "s/tfstatebucket/${environment}-tfstate/" ./provider.tf

  ln -s ../../hcl_template/versions.tf ./
  ln -s ../../hcl_template/main.tf ./
  ln -s ../../hcl_template/vars.tf ./
  ln -s ../../hcl_template/loadbalancer.tf ./
  
    if [ "${dialer}" == "YES" ]; then
    ln -s ../../hcl_template/dialer.tf ./
    ln -s ../../hcl_template/mysql.tf ./
  fi

  if [[ "${type}" == "AIO" && ${dialer} != "YES" ]]; then
    ln -s ../../hcl_template/omlapp_aio_without_dialer.tf ./
  elif [[ "${type}" == "AIO" && ${dialer} == "YES" ]]; then  
    ln -s ../../hcl_template/omlapp_aio.tf ./
  elif [[ "${type}" == "CLUSTER" && ${dialer} != "YES" ]]; then
    ln -s ../../hcl_template/redis.tf ./
    ln -s ../../hcl_template/rtpengine.tf ./
    ln -s ../../hcl_template/kamailio.tf ./
    ln -s ../../hcl_template/pgsql.tf ./
    ln -s ../../hcl_template/asterisk_full_cluster.tf ./
    ln -s ../../hcl_template/websocket.tf ./
    ln -s ../../hcl_template/omlapp_full_cluster_without_dialer.tf ./
    ln -s ../../hcl_template/firewalls/fw_asterisk_without_dialer.tf ./
    ln -s ../../hcl_template/firewalls/fw_kamailio.tf ./
    ln -s ../../hcl_template/firewalls/fw_rtpengine.tf ./
    ln -s ../../hcl_template/firewalls/fw_redis.tf ./
    ln -s ../../hcl_template/firewalls/fw_pgsql.tf ./
    ln -s ../../hcl_template/firewalls/fw_websocket.tf ./
    ln -s ../../hcl_template/firewalls/fw_omlapp.tf ./
  elif [[ "${type}" == "CLUSTER" && ${dialer} == "YES" ]]; then
    ln -s ../../hcl_template/redis.tf ./
    ln -s ../../hcl_template/rtpengine.tf ./
    ln -s ../../hcl_template/kamailio.tf ./
    ln -s ../../hcl_template/pgsql.tf ./
    ln -s ../../hcl_template/asterisk_full_cluster.tf ./
    ln -s ../../hcl_template/websocket.tf ./
    ln -s ../../hcl_template/omlapp_full_cluster.tf ./
    ln -s ../../hcl_template/firewalls/fw_asterisk.tf ./
    ln -s ../../hcl_template/firewalls/fw_kamailio.tf ./
    ln -s ../../hcl_template/firewalls/fw_rtpengine.tf ./
    ln -s ../../hcl_template/firewalls/fw_redis.tf ./
    ln -s ../../hcl_template/firewalls/fw_pgsql.tf ./
    ln -s ../../hcl_template/firewalls/fw_websocket.tf ./
    ln -s ../../hcl_template/firewalls/fw_omlapp.tf ./
  elif [[ "${type}" == "DOCKER" && ${dialer} != "YES" ]]; then
    ln -s ../../hcl_template/rtpengine_docker.tf ./
    ln -s ../../hcl_template/pgsql_docker.tf ./
    ln -s ../../hcl_template/omlapp_docker_notdialer.tf ./
  elif [[ "${type}" == "DOCKER" && ${dialer} == "YES" ]]; then
    ln -s ../../hcl_template/rtpengine_docker.tf ./
    ln -s ../../hcl_template/pgsql_docker.tf ./
    ln -s ../../hcl_template/omlapp_docker.tf ./
  else
    echo " ****************** WARNING invalid DEPLOY TYPE !!! ************************ "  
    echo " the options are: AIO, CLUSTER, FULL_CLUSTER or DOCKER"  
    exit 0
  fi

  sed -i "s/customer-name/$environment/" ./vars.auto.tfvars
  sed -i "s/customer-name/$environment/" ./provider.tf

  sed -i "s/spaces-key-id/$(echo $TF_VAR_spaces_key)/" ./provider.tf
  sed -i "s%spaces-key-secret%$(echo $TF_VAR_spaces_secret_key)%" ./provider.tf
  sed -i "s/spaces-region/$TF_VAR_region/" ./provider.tf

  if [ "${release}" != "" ]; then

    sed -i "s/master/$release/" ./vars.auto.tfvars
  fi

  sleep 2
  #docker run -i -t hashicorp/terraform:light init
  terraform init
}

create_spaces_bucket() {
  local environment=$1
  
  printf "$GREEN** [OMniLeads] *************************************** $NC\n"
  printf "$GREEN** [OMniLeads] Creating Spaces bucket ... 10 seconds * $NC\n"
  printf "$GREEN** [OMniLeads] *************************************** $NC\n"

  BUCKET_TFSTATE=$($S3CMD ls |grep "$environment-tfstate" |awk -F "//" '{print $2}' || true)
  BUCKET_CALLREC=$($S3CMD ls |grep "$environment$" |awk -F "//" '{print $2}' || true)
  
  if [ -z ${BUCKET_TFSTATE} ]; then
    s3cmd mb "s3://${environment}-tfstate"
  fi
  if [ -z ${BUCKET_CALLREC} ]; then
    s3cmd mb "s3://${environment}"
  fi  
  sleep 10  
}

undo_links() {
  links=$(find ${ENVS_DIR}/${1} -type l -path "${ENVS_DIR}/${1}/.terraform/*" -prune -o -type l -print)
  #echo $links
  for file in ${links}; do
    unlink ${file}
  done
}

backup () {

  OMLACD=$(doctl compute droplet list |grep "$1-asterisk" |awk '{ print$3 }')
  OMLKAM=$(doctl compute droplet list |grep "$1-kamailio" |awk '{ print$3 }')
  OMLREDIS=$(doctl compute droplet list |grep "$1-redis" |awk '{ print$3 }')
  OMLAPP=$(doctl compute droplet list |grep $1|grep -v "$1-asterisk" | grep -v "$1-kamailio" | grep -v "$1-redis" | grep -v "$1-rtp" | grep -v "$1-websocket" |awk '{ print$3 }')


  echo "${OMLACD} -- ${OMLKAM} -- ${OMLAPP} -- ${OMLREDIS}"

  scp root@${OMLKAM}:/opt/omnileads/kamailio/etc/kamailio/kamailio.cfg ./${ENVS_DIR}/${1}/backup_restore/
  scp root@${OMLREDIS}:/etc/redis.conf ./${ENVS_DIR}/${1}/backup_restore/
  ssh root@${OMLACD} "cd /opt/omnileads/utils && ./backup-restore.sh --backup --asterisk"
  ssh root@${OMLACD} "cp /opt/omnileads/backup/* /opt/callrec"
  scp root@${OMLACD}:/opt/omnileads/asterisk/etc/asterisk/*_custom.conf ./${ENVS_DIR}/${1}/backup_restore/
  scp root@${OMLACD}:/opt/omnileads/asterisk/etc/asterisk/*_override.conf ./${ENVS_DIR}/${1}/backup_restore/
  scp root@${OMLACD}:/opt/omnileads/asterisk/var/lib/asterisk/agi-bin/*_custom.py ./${ENVS_DIR}/${1}/backup_restore/ 2>&1
  scp root@${OMLAPP}:/opt/omnileads/run/oml_uwsgi.ini ./${ENVS_DIR}/${1}/backup_restore/
  scp root@${OMLAPP}:/etc/nginx/conf.d/ominicontacto.conf ./${ENVS_DIR}/${1}/backup_restore/
  
}  

restore () {

  OMLACD=$(doctl compute droplet list |grep "$1-asterisk" |awk '{ print$3 }')
  OMLKAM=$(doctl compute droplet list |grep "$1-kamailio" |awk '{ print$3 }')
  OMLREDIS=$(doctl compute droplet list |grep "$1-redis" |awk '{ print$3 }')
  OMLAPP=$(doctl compute droplet list |grep $1|grep -v "$1-asterisk" | grep -v "$1-kamailio" | grep -v "$1-redis" | grep -v "$1-rtp" | grep -v "$1-websocket" |awk '{ print$3 }')

  echo "${OMLACD} -- ${OMLKAM} -- ${OMLAPP} -- ${OMLREDIS}"

  scp ./${ENVS_DIR}/${1}/backup_restore/kamailio.cfg root@${OMLKAM}:/opt/omnileads/kamailio/etc/kamailio/
  echo " ****************** restart kamailio ************************ "  
  ssh root@${OMLKAM} systemctl restart kamailio

  scp ./${ENVS_DIR}/${1}/backup_restore/redis.conf root@${OMLREDIS}:/etc/
  echo " ****************** restart redis ************************ "  
  ssh root@${OMLREDIS} systemctl restart redis

  scp ./${ENVS_DIR}/${1}/backup_restore/*_custom.py root@${OMLACD}:/opt/omnileads/asterisk/var/lib/asterisk/agi-bin/ 2>&1
  scp ./${ENVS_DIR}/${1}/backup_restore/*_custom.conf root@${OMLACD}:/opt/omnileads/asterisk/etc/asterisk/
  scp ./${ENVS_DIR}/${1}/backup_restore/*_override.conf  root@${OMLACD}:/opt/omnileads/asterisk/etc/asterisk/
  echo " ****************** dialplan reload asterisk ************************ "  
  ssh root@${OMLACD} "asterisk -rx 'dialplan reload'"
  scp ./${ENVS_DIR}/${1}/backup_restore/oml_uwsgi.ini  root@${OMLAPP}:/opt/omnileads/run/
  scp ./${ENVS_DIR}/${1}/backup_restore/ominicontacto.conf root@${OMLAPP}:/etc/nginx/conf.d/
}  

$@
