#!/bin/bash

set -e

if [ "$1" = "" ]; then
    sed -i "s/access_key_env/$TF_VAR_spaces_key/g" /home/terraform/.s3cfg
    sed -i "s%\access_secret_env%$TF_VAR_spaces_secret_key%g" /home/terraform/.s3cfg
    sed -i "s%\region.digitaloceanspaces.com%$TF_VAR_spaces_region%g" /home/terraform/.s3cfg
    init
fi


