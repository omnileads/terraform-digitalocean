#!/bin/bash
set -e

docker build -f Dockerfile -t freetechsolutions/terraform-do:$1 .
docker push freetechsolutions/terraform-do:$1
